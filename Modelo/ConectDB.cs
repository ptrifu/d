﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class ConectDB
    {
       
        public List<usuario> usuarios;
        public List<incidencia> incidencias;
        public List<material> materials;
        public List<utiliza> utilizas;
        public List<repara> reparas;
        public List<lineaProducto> lineaProductos;


        public ConectDB()
        {
            using (p1Entities db = new p1Entities())//conexion a la base de datos
            {   //cargamos todas las entidades para posteriormente gestionarlas
                usuarios = db.usuario.ToList();
                incidencias = db.incidencia.ToList();
                materials = db.material.ToList();
                utilizas = db.utiliza.ToList();
                reparas = db.repara.ToList();
                lineaProductos = db.lineaProducto.ToList();
            }
        }
    }
}
