﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Presentacion.Models
{
    [DataContract(IsReference = true)]
    public class MaterialDTO
    {
        public long idMaterial { get; set; }
        [Required]
        [Display(Name = "Nombre del material")]
        public string marca { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Precio")]
        public Nullable<decimal> precio { get; set; }
        
        [Display(Name = "Referencia")]
        public string refencia { get; set; }       
        [Display(Name = "Imagen")]
        public byte[] imgMaterial { get; set; }
    }
    [MetadataType(typeof(MaterialDTO))]
    
    public partial class material
    {
        public string referenciaBuena { get; set; } = String.Format("MM-{0}-CC", DateTime.Now.ToString("MMddyyyyhhmmssffftt"));
       
    }

    
}