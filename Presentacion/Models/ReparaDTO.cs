﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentacion.Models
{
    public class ReparaDTO
    {
        public long idRepara { get; set; }
        [Required]
        [Display(Name = "Tecnico")]
        public long idUsuario { get; set; }
        [Required]
        [Display(Name = "Incidencia")]
        public long idIncidencia { get; set; }
        public Nullable<long> idLineaProducto { get; set; }
        [Required]
        [Display(Name = "Descripcion")]
        public string descripcionAveria { get; set; }
        
        [DataType(DataType.Date)]        
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> fecha { get; set; }
        [Required]
        [Display(Name = "Horas Trabajadas")]
        public string horasTrabajadas { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Precio Hora")]
        public string precioHora { get; set; }
    }
    [MetadataType(typeof(ReparaDTO))]
    public partial class repara {
        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime fechaBuena
        {

            get
            {
                var fechas = DateTime.Now;
                if (fecha != null)
                {
                    fechas = (DateTime)fecha;
                }


                return fechas;
            }
        }


        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime fechaHoy { get; set; } = DateTime.Now;
    }
}