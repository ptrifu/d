﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentacion.Models
{
    public class UsuarioDTO
    {

        [Display(Name = "Id Usuario")]
        public long idUsuario { get; set; }
        [Required]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }
        [Required]
        [Display(Name = "DNI")]
        public string dni { get; set; }
        [Required]
        [Display(Name = "Apellido")]
        public string apellido { get; set; }
   
        [Display(Name = "Segundo Apellido")]
        public string segundoApellido { get; set; }
  
        [Display(Name = "Telefono")]
        public string telefono { get; set; }
     
        [Display(Name = "Tecnico")]
        public bool tipo { get; set; }
        [Required]
        [Display(Name = "Usuario")]
        public string usu { get; set; }
        [Required]
        [Display(Name = "Contraseña")]
        public string contra { get; set; }
        [Required]
        [Display(Name = "Correo")]
        public string correo { get; set; }
    }
    [MetadataType(typeof(UsuarioDTO))]
    public partial class usuario {

       

    }
}