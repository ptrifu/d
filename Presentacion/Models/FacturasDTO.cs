﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Runtime.Serialization;
using System.Web;

namespace Presentacion.Models
{
    [DataContract(IsReference = true)]
    public class FacturasDTO
    {
        //public List<usuario> Usu { get; set; }
        //public List<material>  Mate { get; set; }

        public usuario Usu { get; set; } = new usuario();
        public material Mate { get; set; } = new material();

        public lineaProducto LineaPro { get; set; } = new lineaProducto();

        public incidencia Inci { get; set; } = new incidencia();


        public List<lineaProducto> lineaProductoList { get; set; }
        public long idUsuario { get; set; }

        public GetDatosFacturaDesdeIncidencia_Result datosPro {get; set;}

        public List<MaterialDTO> allMate { get; set; }

        public GetLineasByIncidecia_Result datosTotales { get; set; }
        
        public Boolean PDF { get; set; }

        public decimal Total { get; set; }

        public decimal totalMaterial { get; set; }

        public decimal porImpue { get; set; }
        public string numFact { get; set; } 

    }
}