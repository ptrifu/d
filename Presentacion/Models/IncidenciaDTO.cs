﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentacion.Models
{
    public class IncidenciaDTO
    {
        [Required]
        [Display(Name = "Descripcion Incidencia")]
        public string descripcionIncidencia { get; set; }
        
        [DataType(DataType.Date)]        
        [Display(Name = "Fecha Incidencia")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> fechaIncidencia { get; set; }        
        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Required]
        [Display(Name = "Cliente")]
        public Nullable<long> idUsuario { get; set; }
    }
    [MetadataType(typeof(IncidenciaDTO))]
    public partial class incidencia
    {

        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime fechaBuena
        {
            
            get
            {
                var fecha = DateTime.Now;
                if (fechaIncidencia != null) {
                    fecha = (DateTime)fechaIncidencia;
                }
                

                return fecha;
            }
        }


        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime  fechaHoy { get; set; } = DateTime.Now;


        public string FullDescription
        {
            get { return string.Format("{0} - {1} - {2} - {3} ", this.descripcionIncidencia, this.fechaIncidencia,this.estado,this.usuario.nombre); }
        }
    }
}