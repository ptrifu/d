﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Presentacion.Models
{
    [DataContract(IsReference = true)]
    public class lineaProductoDTO
    {
        public long idLineaProducto { get; set; }
        [Required]
        [Display(Name = "Cantidad")]
        public string numeroMaterial { get; set; }
        [Display(Name = "Precio Unidad")]
        public string precioMaterial { get; set; }
        public long idUtiliza { get; set; }
        public Nullable<long> idRepara { get; set; }
        [Required]
        [Display(Name = "Material")]
        public Nullable<long> idMaterial { get; set; }

        public virtual material material { get; set; }
        public virtual repara repara { get; set; }
        public virtual utiliza utiliza { get; set; }
    }
    
    [MetadataType(typeof(lineaProductoDTO))]
    public partial class lineaProducto
    {

        


        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime fechaHoy { get; set; } = DateTime.Now;


        
    }
}