﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentacion.Models
{
    public class GetDatosFacturaDesdeIncidenciaDTO
    {
        public long idIncidencia { get; set; }
        [Display(Name = "Descripcion Incidencia")]
        public string descripcionIncidencia { get; set; }
        [Display(Name = "Fecha Incidencia")]
        public Nullable<System.DateTime> fechaIncidencia { get; set; }
        public string estado { get; set; }
        public long idUsuarioCliente { get; set; }
        public string nombreCliente { get; set; }
        public string apellidoCliente { get; set; }
        public string segundoApellidoCliente { get; set; }
        [Display(Name = "Nº Telefono")]
        public string telefonoCliente { get; set; }
        public bool tipoCliente { get; set; }
        [Display(Name = "Correo")]
        public string correoCliente { get; set; }
        public long idRepara { get; set; }
        public string descripcionAveria { get; set; }
        [Display(Name = "Fecha Repacion")]
        public Nullable<System.DateTime> fechaReparacion { get; set; }
        [Display(Name = "Horas Trabajadas")]
        public string horasTrabajadas { get; set; }
        [Display(Name = "Precio Hora")]
        public string precioHora { get; set; }
        public long idUsuarioTecnico { get; set; }
        public string nombreTecnico { get; set; }
        public string apellidoTecnico { get; set; }
        public string segundoApellidoTecnico { get; set; }
        [Display(Name = "Nº Telefono")]
        public string telefonoTecnico { get; set; }
        public bool tipoTecnico { get; set; }
        
        [Display(Name = "Correo")]
        public string correoTecnico { get; set; }
      
        [Display(Name = "Nombre Cliente")]
        public string NombreCompletoCliente { get; set; }
        
        [Display(Name = "Nombre Tecnico")]
        public string NombreCompletoTecnico { get; set; }
    }

    [MetadataType(typeof(GetDatosFacturaDesdeIncidenciaDTO))]
    public partial class GetDatosFacturaDesdeIncidencia_Result
    {


    }



}