﻿using Microsoft.Ajax.Utilities;
using Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services.Description;
using System.Web.UI.WebControls.WebParts;

namespace Presentacion.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index(int id = 0)
        {
            try
            {
                if (id != 0)
                {
                    DataBase db = new DataBase();
                    var usu = db.usuarios.Where(usua => usua.idUsuario == id).FirstOrDefault();
                    return View(usu);
                }
                else {
                 
                    var xxx = Session["id"];
                    int IdUsuario = Convert.ToInt32(xxx);
                    DataBase db = new DataBase();
                    var usu = db.usuarios.Where(usua => usua.idUsuario == IdUsuario).FirstOrDefault();
                    return View(usu);
                }
            }           
            catch (Exception)
            {

                throw;
            }
          
        }
        [Authorize]
        public ActionResult MostrarTodo()
        {
            try
            {
                DataBase db = new DataBase();
                var usu = db.usuarios;
                return View(usu);
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        [Authorize]
        public ActionResult CrearUsuario()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CrearUsuario(usuario usua) {
            if (ModelState.IsValid)
            {
                try
                {

                   // DataBase db = new DataBase();
                   //db.usuarios.Add(usua);
                    using (var datos = new SoloOneEntities())//conexion a la base de datos
                    {   //cargamos todas las entidades para posteriormente gestionarlas
                        datos.usuario.Add(usua);
                        datos.SaveChanges();
                    }


                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Error al crear usuario" + ex.Message);
                    return RedirectToAction("MostrarTodo");
                }
                return RedirectToAction("MostrarTodo");
            }
            else {
                return View();
            }
           
        }
        [Authorize]
        public ActionResult EditUsuario(int id = 0) {
            try
            {
                DataBase db = new DataBase();
                var usu = db.usuarios.Where(u => u.idUsuario == id).FirstOrDefault();
                //var usu = db.usuarios.Find(id);
                return View(usu);
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult EditUsuario(usuario usua)
        {
            
            try
            {

                if (!ModelState.IsValid)
                    return View();
                using (var datos = new SoloOneEntities())//conexion a la base de datos
                {   //cargamos todas las entidades para posteriormente gestionarlas
                    var usumodi = datos.usuario.Find(usua.idUsuario);
                    usumodi = usua;
                    datos.usuario.AddOrUpdate(usumodi);
                    datos.SaveChanges();
                    return RedirectToAction("MostrarTodo");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }
        [Authorize]
        public ActionResult DetalleUsuario(int id = 0)
        {
            if (id == 0) {
                return RedirectToAction("MostrarTodo");
            }
            try
            {
                DataBase db = new DataBase();
                var usu = db.usuarios.Where(u => u.idUsuario == id).FirstOrDefault();
                //var usu = db.usuarios.Find(id);
                return View(usu);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [Authorize]
        public ActionResult BorrarUsuario(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("MostrarTodo");
            }
            try
            {
                using (var datos = new SoloOneEntities())//conexion a la base de datos
                {   //cargamos todas las entidades para posteriormente gestionarlas
                    var usuborrar = datos.usuario.Find(id);
                    
                    datos.usuario.Remove(usuborrar);
                    datos.SaveChanges();
                    return RedirectToAction("MostrarTodo");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
    


}