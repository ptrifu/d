﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Presentacion.Models;

namespace Presentacion.Controllers
{
    public class ReparaController : Controller
    {
        private SoloOneEntities db = new SoloOneEntities();

        // GET: Repara
        [Authorize]
        public ActionResult Index()
        {
            var repara = db.repara.Include(r => r.incidencia).Include(r => r.usuario);
            return View(repara.ToList());
        }

        // GET: Repara/Details/5
        [Authorize]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            repara repara = db.repara.Find(id);
            if (repara == null)
            {
                return HttpNotFound();
            }
            return View(repara);
        }

        // GET: Repara/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.idIncidencia = new SelectList(db.incidencia.Where(inci => inci.estado.ToUpper() != "HECHO"), "idIncidencia", "descripcionIncidencia");
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == true), "idUsuario", "nombre");
            ViewBag.fechaHoy = DateTime.Now.ToShortDateString();
            return View();
        }

        // POST: Repara/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "idRepara,idUsuario,idIncidencia,idLineaProducto,descripcionAveria,fecha,horasTrabajadas,precioHora")] repara repara)
        {
            if (ModelState.IsValid)
            {
                repara.fecha = repara.fechaHoy;
                db.repara.Add(repara);
                db.SaveChanges();
                ActulizarEstadoIncidencia(repara.idIncidencia, true);
                return RedirectToAction("Index");
            }

            ViewBag.idIncidencia = new SelectList(db.incidencia, "idIncidencia", "descripcionIncidencia", repara.idIncidencia);
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == true), "idUsuario", "nombre");
            return View(repara);
        }

        // GET: Repara/Edit/5
        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            repara repara = db.repara.Find(id);
            if (repara == null)
            {
                return HttpNotFound();
            }            
            ViewBag.idIncidencia = new SelectList(db.incidencia, "idIncidencia", "descripcionIncidencia", repara.idIncidencia);
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == true), "idUsuario", "nombre");
            return View(repara);
        }

        // POST: Repara/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "idRepara,idUsuario,idIncidencia,idLineaProducto,descripcionAveria,fecha,horasTrabajadas,precioHora,fechaBuena")] repara repara)
        {
            if (ModelState.IsValid)
            {
                repara.fecha = repara.fechaBuena;
                db.Entry(repara).State = EntityState.Modified;
                db.SaveChanges();
                ActulizarEstadoIncidencia(repara.idIncidencia, true);
                return RedirectToAction("Index");
            }
            ViewBag.idIncidencia = new SelectList(db.incidencia, "idIncidencia", "descripcionIncidencia", repara.idIncidencia);
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == true), "idUsuario", "nombre");
            return View(repara);
        }

        // GET: Repara/Delete/5
        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            repara repara = db.repara.Find(id);
            if (repara == null)
            {
                return HttpNotFound();
            }
            return View(repara);
        }

        // POST: Repara/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(long id)
        {
            repara repara = db.repara.Find(id);
            db.repara.Remove(repara);
            db.SaveChanges();
            ActulizarEstadoIncidencia(repara.idIncidencia, false);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void ActulizarEstadoIncidencia(long id,bool estado) {
            var incidencias = db.incidencia.Where(x => x.idIncidencia == id).FirstOrDefault();
            if (estado)
            {
                incidencias.estado = "Hecho";
            }
            else 
            { 
                incidencias.estado = "Pendiente"; 
            }
            using (var datos = new SoloOneEntities())//conexion a la base de datos
            {   //cargamos todas las entidades para posteriormente gestionarlas
                
                datos.incidencia.AddOrUpdate(incidencias);
                datos.SaveChanges();
            }
        }
    }
}
