﻿using Microsoft.Ajax.Utilities;
using Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services.Description;

namespace Presentacion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string usuario, string password)
        {

            if (!string.IsNullOrEmpty(usuario) && !string.IsNullOrEmpty(password))
            {
                DataBase dB = new DataBase();
                var usua = dB.usuarios.FirstOrDefault(u => u.usu == usuario && u.contra == password);
                if (usua != null)
                {
                    FormsAuthentication.SetAuthCookie(usua.usu, true);
                    var id = usua.idUsuario;
                    string userData = usua.idUsuario.ToString();
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, usua.usu, DateTime.Now, DateTime.Now.AddMinutes(30), true, userData);
                    string encTicket = FormsAuthentication.Encrypt(ticket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    System.Web.HttpContext.Current.Response.Cookies.Add(faCookie);
                    Session.Add("id", id);
                   
                    return RedirectToAction("Factu", "Home");
                }
                else
                {
                    ViewBag.Message = "Datos erroneos";
                    return View();
                }
            }
            return View();

            
        }


        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Sobre nosotros.";

            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Conacto.";

            return View();
        }


        //[HttpPost]        
        public ActionResult IniciarSesion(string usuario, string password)
        {
            if (!string.IsNullOrEmpty(usuario) && !string.IsNullOrEmpty(password))
            {
                DataBase dB = new DataBase();
                var usua = dB.usuarios.FirstOrDefault(u => u.usu == usuario && u.contra == password);
                if (usua != null)
                {
                    FormsAuthentication.SetAuthCookie(usua.usu, true);
                    return RedirectToAction("Index", "Profile",new {id = usua.idUsuario });
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
           
            return View();


        }

        [Authorize]
        public ActionResult CerrarSesion() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Factu() {
           var nombreusus = System.Web.HttpContext.Current.User.Identity.Name;
            ViewBag.nombre = nombreusus;
            return View();
        }

       
        

    }
}