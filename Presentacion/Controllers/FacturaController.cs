﻿using Antlr.Runtime.Misc;
using NReco.PdfGenerator;
using OpenHtmlToPdf;
using Presentacion.Models;
using RazorEngine;
using RazorEngine.Compilation.ImpromptuInterface;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;


namespace Presentacion.Controllers
{
    public class FacturaController : Controller
    {
        private SoloOneEntities db = new SoloOneEntities();
        // GET: Factura
        [Authorize]
        public ActionResult Index()
        {

            var fac = new FacturasDTO();
            fac.lineaProductoList = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza).ToList();

            ViewBag.lis = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza).ToList();
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == true), "idUsuario", "nombre");
            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca");
            ViewBag.idIncidencia = new SelectList(db.incidencia, "idIncidencia", "FullDescription");
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria");
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza");
            return View(fac);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Index(FacturasDTO a, [Bind(Include = "idLineaProducto,numeroMaterial,precioMaterial,idUtiliza,idRepara,idMaterial")] lineaProducto lineaProducto)
        {

            if (ModelState.IsValid)
            {
                db.lineaProducto.Add(lineaProducto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca", lineaProducto.idMaterial);
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria", lineaProducto.idRepara);
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza", lineaProducto.idUtiliza);

            var fac = new FacturasDTO();
            fac.lineaProductoList = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza).ToList();
            return View();
        }
        [Authorize]
        public ActionResult SeleccionIncidencia()
        {
            ViewBag.idIncidencia = new SelectList(db.incidencia.Where(i => i.estado.ToUpper().Equals("HECHO")), "idIncidencia", "FullDescription");
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult SeleccionIncidencia(int idIncidencia) {
            //List<GetDatosFacturaDesdeIncidencia_Result> x = db.GetDatosFacturaDesdeIncidencia(idIncidencia).ToList();
            //TempData["Datos"] = x;           
            return RedirectToAction("Facturar", new { id = idIncidencia });
        }
        [Authorize]
        public ActionResult Facturar(int id) {

            //List<GetDatosFacturaDesdeIncidencia_Result> x = (List<GetDatosFacturaDesdeIncidencia_Result>)TempData["Datos"];

            List<GetDatosFacturaDesdeIncidencia_Result> x = db.GetDatosFacturaDesdeIncidencia(id).ToList();
            var fac = new FacturasDTO();
            fac.datosPro = x.FirstOrDefault();
            fac.Inci.idIncidencia = id;
            fac.lineaProductoList = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza).Where(l => l.idRepara == fac.datosPro.idRepara).ToList();
            fac.LineaPro = new lineaProducto();
            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca");
            ViewBag.idRepara = new SelectList(db.repara.Where(r => r.idRepara == fac.datosPro.idRepara), "idRepara", "descripcionAveria");
            var nueva = new List<MaterialDTO>();

            foreach (var item in db.material.ToList())
            {               
                var prueb = new MaterialDTO();
                prueb.idMaterial = item.idMaterial;
                prueb.precio = item.precio;
                nueva.Add(prueb);
            }
            fac.allMate = nueva;
            return View(fac);
        }
        [Authorize]
        public ActionResult SetLineas() {

            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SetLineas([Bind(Include = "idLineaProducto,numeroMaterial,precioMaterial,idUtiliza,idRepara,idMaterial")] lineaProducto lineaProductos, int id)
        {
            var idIncidencia = db.repara.Where(r => r.idRepara == id).Select(x => x.idIncidencia).FirstOrDefault();
            if (ModelState.IsValid)
            {

                var precioMaterial = db.material.Where(mat => mat.idMaterial == lineaProductos.idMaterial).FirstOrDefault().precio;
                lineaProductos.precioMaterial = precioMaterial.ToString();
                lineaProductos.idRepara = id;
                lineaProductos.idUtiliza = 1;
                db.lineaProducto.Add(lineaProductos);
                db.SaveChanges();

                return RedirectToAction("Facturar", new { id = idIncidencia });



            }
            ViewBag.ErrorMessage = "Material y cantidad de material obligatorio";

            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca", lineaProductos.idMaterial);
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria", lineaProductos.idRepara);
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza", lineaProductos.idUtiliza);
            return RedirectToAction("Facturar", new { id = idIncidencia });
        }

        [Authorize]
        public ActionResult CalculosTotalesFac(int id, string s = "")
        {
            FacturasDTO fac = DatosFactura(id);
            fac.PDF = false;            
            return View(fac);


        }
       
      
        //Para 

        [HttpPost]
        [Authorize]
        public ActionResult CalculosTotalesFac(int id) {

            FacturasDTO fac = DatosFactura(id);
            fac.PDF = false;
            return View(fac);
        }
        //Para imprimir pdf
        [Authorize]
        public ActionResult Pdf1(int idInci)
        {

            var x = new Rotativa.MVC.ActionAsPdf("CalculosTotalesFac", new { id = idInci }) { FileName = "Test.pdf" };
            var aa = x.BuildPdf(ControllerContext);
            FileResult fileResult = new FileContentResult(aa, "application/pdf");
            fileResult.FileDownloadName = "ThisMvcViewToPdf.pdf";
            return fileResult;
        }

        [Authorize]
        public ActionResult Pdf2(int idInci)
        {
            FacturasDTO fac = DatosFactura(idInci);
            fac.PDF = true;

            string htmlString = HtmlEnString("CalculosTotalesFac", fac);
            string baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();



            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            var inci = db.incidencia.Where(incide => incide.idIncidencia == idInci).FirstOrDefault();
            inci.facPdf = pdf;
            db.SaveChanges();
            
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;           

        }
        [Authorize]
        public ActionResult Pdf3(int idInci)
        {

            FacturasDTO fac = DatosFactura(idInci);
            fac.PDF = true;
            string html = HtmlEnString("CalculosTotalesFac", fac);
            ViewData.Add("TxtHtmlCode", html);
            return View();
        }
        [Authorize]
        private FacturasDTO DatosFactura(int id)
        {
            List<GetLineasByIncidecia_Result> ListaLineas = db.GetLineasByIncidecia(id).ToList();
            GetDatosFacturaDesdeIncidencia_Result DatosGeneral = db.GetDatosFacturaDesdeIncidencia(id).FirstOrDefault();
            var fac = new FacturasDTO();
            fac.datosPro = DatosGeneral;
            fac.Inci.idIncidencia = id;
            fac.lineaProductoList = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza).Where(l => l.idRepara == fac.datosPro.idRepara).ToList();
            var repa = db.repara.Where(rep => rep.idIncidencia == id).FirstOrDefault();
            var x = new lineaProducto();
            x.material = new material();
            x.material.marca = "Mano de obra";
            x.precioMaterial = repa.precioHora;
            x.numeroMaterial = repa.horasTrabajadas;
            fac.lineaProductoList.Add(x);
            var totalDeLinesMaterial = new Decimal();
            foreach (var item in ListaLineas)
            {
                totalDeLinesMaterial += (Decimal)item.PrecioTotalMaterial;

            }
            var totalMano = Convert.ToDecimal(repa.precioHora.Replace(".", ",")) * Convert.ToDecimal(repa.horasTrabajadas.Replace(".", ","));
            var totalMateMano = totalMano + totalDeLinesMaterial;
            var impuesto = 21;
            var PorcentajeImpuesto = (totalMateMano * impuesto) / 100;
            fac.porImpue = PorcentajeImpuesto;
            fac.totalMaterial = totalMateMano;
            fac.Total = totalMateMano + PorcentajeImpuesto;
            Random r = new Random();
            var numer = r.Next(0, 1000000);            
            fac.numFact = numer.ToString("000000");
            return fac;
        }
        [Authorize]
        public string HtmlEnString(string viewName, object model)
        {
            ViewData.Model = model;
            //using (var sw = new StringWriter())
            //{
            //    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
            //    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
            //    viewResult.View.Render(viewContext, sw);
            //    viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
            //    return sw.GetStringBuilder().ToString();
            //}



            // create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            // get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext,
                      viewName,null);
            // create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                new ViewDataDictionary(model),
                new TempDataDictionary(),
                stringWriter
            );

            // render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();
        }

        
        [Authorize]
        public ActionResult BorrarLinea(long id,int idRepa)
        {
            var idIncidencia = db.repara.Where(repa => repa.idRepara == idRepa).Select(r => r.idIncidencia).FirstOrDefault();
           
            lineaProducto lineaProducto = db.lineaProducto.Find(id);
            db.lineaProducto.Remove(lineaProducto);
            db.SaveChanges();

            return RedirectToAction("Facturar", new { id = idIncidencia });
        }




    }
}