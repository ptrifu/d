﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Presentacion.Models;

namespace Presentacion.Controllers
{
    public class MaterialController : Controller
    {
        private SoloOneEntities db = new SoloOneEntities();

        // GET: Material
        [Authorize]
        public ActionResult Index()
        {
            return View(db.material.ToList());
        }

        // GET: Material/Details/5
        [Authorize]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            material material = db.material.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }
        [Authorize]
        // GET: Material/Create
        public ActionResult Create()
        {
            ViewBag.ReferenciaBuena = String.Format("MM-{0}-CC", DateTime.Now.ToString("MMddyyyyhhmmssffftt"));
            return View();
        }

        // POST: Material/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "idMaterial,marca,precio,refencia,imgMaterial")] material material)
        {
            HttpPostedFileBase img = Request.Files[0];
            if (img.ContentLength == 0)
            {
                ModelState.AddModelError("Imagen", "Es necesario seleccionar una imaegen.");
                
            }
            else {
                if (img.FileName.EndsWith(".png") || img.FileName.EndsWith(".jpg"))
                {
                    WebImage img2 = new WebImage(img.InputStream);
                    material.imgMaterial = img2.GetBytes();
                }
                else
                {
                    ModelState.AddModelError("Imagen", "Solo imagenes con formato .jpg y .png.");
                }
            }
           
            if (ModelState.IsValid)
            {
                material.refencia = material.referenciaBuena;                
                db.material.Add(material);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(material);
        }

        // GET: Material/Edit/5
        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            material material = db.material.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }

        // POST: Material/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "idMaterial,marca,precio,refencia,imgMaterial")] material material)
        {
            byte[] imgActual = db.material.Find(material.idMaterial).imgMaterial; 
            string refe = db.material.Find(material.idMaterial).refencia;

            HttpPostedFileBase img = Request.Files[0];
            if (img.ContentLength == 0)
            {                
                material.imgMaterial = imgActual;
            }
            else {
                if (img.FileName.EndsWith(".png") || img.FileName.EndsWith(".jpg"))
                {
                    WebImage img2 = new WebImage(img.InputStream);
                    material.imgMaterial = img2.GetBytes();
                }
                else
                {
                    ModelState.AddModelError("Imagen", "Solo imagenes con formato .jpg y .png.");
                   
                    material.imgMaterial = imgActual;

                }
            }
           

            if (ModelState.IsValid)
            {
                material.refencia = refe;
                db.material.AddOrUpdate(material);
                db.SaveChanges();
                //db.Entry(material).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(material);
        }

        // GET: Material/Delete/5
        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            material material = db.material.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }

        // POST: Material/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(long id)
        {
            material material = db.material.Find(id);
            db.material.Remove(material);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult getImg(int id)
        {
            var material = db.material.Find(id);
            byte[] byteImg = material.imgMaterial;
            MemoryStream memoryStream = new MemoryStream(byteImg);
            Image img = Image.FromStream(memoryStream);

            memoryStream = new MemoryStream();
            img.Save(memoryStream, ImageFormat.Png);
            memoryStream.Position = 0;
            return File(memoryStream,"img/png");
        }

    }
}
