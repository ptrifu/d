﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Presentacion.Models;

namespace Presentacion.Controllers
{
    public class IncidenciaController : Controller
    {
        private SoloOneEntities db = new SoloOneEntities();

        [Authorize]
        // GET: Incidencia
        public ActionResult Index()
        {
            var incidencia = db.incidencia.Include(i => i.usuario);
            return View(incidencia.ToList());
        }
        [Authorize]
        // GET: Incidencia/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            incidencia incidencia = db.incidencia.Find(id);
            if (incidencia == null)
            {
                return HttpNotFound();
            }
            return View(incidencia);
        }
        [Authorize]
        // GET: Incidencia/Create
        public ActionResult Create()
        {
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == false), "idUsuario", "nombre");            
            ViewBag.fechaHoy = DateTime.Now.ToShortDateString();
            return View();
        }

        // POST: Incidencia/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "idIncidencia,descripcionIncidencia,fechaIncidencia,estado,idUsuario")] incidencia incidencia)
        {
            if (ModelState.IsValid)
            {
                incidencia.estado = "Pendiente";
                incidencia.fechaIncidencia = incidencia.fechaHoy;
                db.incidencia.Add(incidencia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == false), "idUsuario", "nombre");
            return View(incidencia);
        }

        // GET: Incidencia/Edit/5
        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            incidencia incidencia = db.incidencia.Find(id);
            if (incidencia == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == false), "idUsuario", "nombre");
            return View(incidencia);
        }

        // POST: Incidencia/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "idIncidencia,descripcionIncidencia,fechaIncidencia,estado,idUsuario,fechaBuena")] incidencia incidencia)
        {
            if (ModelState.IsValid)
            {
                incidencia.fechaIncidencia = incidencia.fechaBuena;
                db.Entry(incidencia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idUsuario = new SelectList(db.usuario.Where(usu => usu.tipo == false), "idUsuario", "nombre");
            return View(incidencia);
        }
        [Authorize]
        // GET: Incidencia/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            incidencia incidencia = db.incidencia.Find(id);
            if (incidencia == null)
            {
                return HttpNotFound();
            }
            return View(incidencia);
        }

        // POST: Incidencia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(long id)
        {
            incidencia incidencia = db.incidencia.Find(id);
            db.incidencia.Remove(incidencia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
