﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Presentacion.Models;

namespace Presentacion.Controllers
{
    public class LineaProductoController : Controller
    {
        private SoloOneEntities db = new SoloOneEntities();

        [Authorize]
        // GET: LineaProducto
        public ActionResult Index()
        {
            var lineaProducto = db.lineaProducto.Include(l => l.material).Include(l => l.repara).Include(l => l.utiliza);
            return View(lineaProducto.ToList());
        }

        // GET: LineaProducto/Details/5
        [Authorize]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lineaProducto lineaProducto = db.lineaProducto.Find(id);
            if (lineaProducto == null)
            {
                return HttpNotFound();
            }
            return View(lineaProducto);
        }
        [Authorize]
        // GET: LineaProducto/Create
        public ActionResult Create()
        {
            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca");
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria");
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza");
            return View();
        }

        // POST: LineaProducto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "idLineaProducto,numeroMaterial,precioMaterial,idUtiliza,idRepara,idMaterial")] lineaProducto lineaProducto)
        {
            if (ModelState.IsValid)
            {
                db.lineaProducto.Add(lineaProducto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca", lineaProducto.idMaterial);
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria", lineaProducto.idRepara);
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza", lineaProducto.idUtiliza);
            return View(lineaProducto);
        }

        // GET: LineaProducto/Edit/5
        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lineaProducto lineaProducto = db.lineaProducto.Find(id);
            if (lineaProducto == null)
            {
                return HttpNotFound();
            }
            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca", lineaProducto.idMaterial);
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria", lineaProducto.idRepara);
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza", lineaProducto.idUtiliza);
            return View(lineaProducto);
        }

        // POST: LineaProducto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "idLineaProducto,numeroMaterial,precioMaterial,idUtiliza,idRepara,idMaterial")] lineaProducto lineaProducto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lineaProducto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idMaterial = new SelectList(db.material, "idMaterial", "marca", lineaProducto.idMaterial);
            ViewBag.idRepara = new SelectList(db.repara, "idRepara", "descripcionAveria", lineaProducto.idRepara);
            ViewBag.idUtiliza = new SelectList(db.utiliza, "idUtiliza", "idUtiliza", lineaProducto.idUtiliza);
            return View(lineaProducto);
        }

        // GET: LineaProducto/Delete/5
        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lineaProducto lineaProducto = db.lineaProducto.Find(id);
            if (lineaProducto == null)
            {
                return HttpNotFound();
            }
            return View(lineaProducto);
        }

        // POST: LineaProducto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(long id)
        {
            lineaProducto lineaProducto = db.lineaProducto.Find(id);
            db.lineaProducto.Remove(lineaProducto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
